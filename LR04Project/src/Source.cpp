#include <opencv2\highgui.hpp>
#include <iostream>
#include <opencv2\core.hpp>
#include <opencv2\core\types_c.h>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui_c.h>

using namespace cv;
using namespace std;

Mat histogram(Mat img)
{
	int hist[256]{ 0 };
	for (int i = 0; i < img.rows; i++) {
		for (int j = 0; j < img.cols; j++) {
			hist[img.at<uint8_t>(i, j)]++;
		}
	}
	int n = sizeof(hist) / sizeof(hist[0]);
	int max;
	max = *max_element(hist, hist + n); //     
	Mat hist_img(Mat::zeros(400, 256 * 2, CV_8UC1));
	hist_img.setTo(255);
	for (int i = 0; i < 256; i++) {
		rectangle(hist_img, Point(i * 2, 400 - int(float(hist[i]) / max * 400)), Point(i * 2 + 2, 400), Scalar::all(0), -1);
	}
	return hist_img;
}


void thresholdIntegral(Mat &inputMat, Mat &outputMat, float T, int part)
{
	int x_1, y_1, x_2, y_2, count, sum, i_p, i_t, S, s_2;
	Mat sum_Mat;
	int *p_y1, *p_y2;
	uchar *p_inputMat, *p_outputMat;

	integral(inputMat, sum_Mat);

	S = MAX(inputMat.rows, inputMat.cols);
	S = (S > (part + part)) ? (S / part) : 2;

	s_2 = S / 2;

	for (int i = 0; i < inputMat.rows; ++i)
	{
		y_1 = (i < s_2) ? 0 : (i - s_2);
		y_2 = ((i + s_2) < inputMat.rows) ? (i + s_2) : (inputMat.rows - 1);

		p_y1 = sum_Mat.ptr<int>(y_1);
		p_y2 = sum_Mat.ptr<int>(y_2);
		p_inputMat = inputMat.ptr<uchar>(i);
		p_outputMat = outputMat.ptr<uchar>(i);

		for (int j = 0; j < inputMat.cols; ++j)
		{
			x_1 = (j < s_2) ? 0 : (j - s_2);
			x_2 = ((j + s_2) < inputMat.cols) ? (j + s_2) : (inputMat.cols - 1);

			count = (x_2 - x_1) * (y_2 - y_1);

			sum = p_y2[x_2] - p_y1[x_2] - p_y2[x_1] + p_y1[x_1];

			i_p = (int)(p_inputMat[j] * count);
			i_t = (int)(sum * (1.0f - T));
			p_outputMat[j] = (i_p < i_t) ? 0 : 255;
		}
	}
}

int otsuThreshold(Mat image)
{
	int min = image.at<uint8_t>(0, 0);
	int max = image.at<uint8_t>(0, 0);

	for (int i = 1; i < image.rows; i++)
	{
		for (int j = 0; j < image.cols; j++) {
			int value = image.at<uint8_t>(i, j);
			if (value < min)
				min = value;

			if (value > max)
				max = value;
		}
	}

	int hist_size = max - min + 1;
	int* hist = new int[hist_size];

	for (int t = 0; t < hist_size; t++)
		hist[t] = 0;

	for (int i = 0; i < image.rows; i++)
		for (int j = 0; j < image.cols; j++) {
			hist[image.at<uint8_t>(i, j) - min]++;
		}

	int m = 0;
	int n = 0;
	for (int t = 0; t <= max - min; t++)
	{
		m += t * hist[t];
		n += hist[t];
	}

	float max_sigma = -1;
	int threshold = 0;

	int alpha_1 = 0;
	int beta_1 = 0;

	for (int t = 0; t < max - min; t++)
	{
		alpha_1 += t * hist[t];
		beta_1 += hist[t];

		float w_1 = (float)beta_1 / n;

		float a = (float)alpha_1 / beta_1 - (float)(m - alpha_1) / (n - beta_1);

		float sigma = w_1 * (1 - w_1) * a * a;

		if (sigma > max_sigma)
		{
			max_sigma = sigma;
			threshold = t;
		}
	}

	threshold += min;

	return threshold;
}


int main() {
	//histogram
	Mat img = imread("image.jpg");
	imshow("Image", img);
   
	Mat hist = histogram(img);

	imshow("histogram", hist);
	imwrite("histogram.jpg", hist);

	//Table function
	float gamma_LUT = 2.0;

	Mat lookupTable(1, 256, CV_8U);
	uchar* lut = lookupTable.ptr();
	for (int i = 0; i < 256; i++) {
		lut[i] = int(255 * (pow((float)i / 255.0, gamma_LUT)));
	}
	Mat graph = Mat(256, 256 * 2, CV_8UC1, Scalar(255, 255, 255));
	for (int i = 0; i < 256 - 1; i++) {
		line(graph, Point(i * 2, 256 - lookupTable.at<uint8_t>(0, i)), Point(i * 2 + 2, 256 - lookupTable.at<uint8_t>(0, i + 1)), Scalar::all(0), 1);
	}

	imshow("Graph", graph);
	imwrite("graph.jpg", graph);

	//Second histogram
	Mat img_1;
	LUT(img, lookupTable, img_1);
	imshow("Changed image", img_1);
	imwrite("Changed_img.jpg", img_1);
	Mat second_hist = histogram(img_1);
	imshow("Histogram after table function", second_hist);
	imwrite("Changed_histogram.jpg", second_hist);


	//CLAHE
	Mat clahe_img;
	Ptr<CLAHE> clahe = createCLAHE();
	cvtColor(img, img, COLOR_BGR2GRAY);
	clahe->setClipLimit(4);
	clahe->setTilesGridSize(Size(5, 5));
	clahe->apply(img, clahe_img);
	Mat clahe_hist_1 = histogram(clahe_img);
	imshow("Histogram for CLAHE 4 (5,5)", clahe_hist_1);
	imwrite("CLAHE_histogram_4(5,5).jpg", clahe_hist_1);
	imshow("�lahe 4 (5,5)", clahe_img);
	imwrite("CLAHE_1.jpg", clahe_img);

	clahe->setClipLimit(0.5);
	clahe->setTilesGridSize(Size(10, 10));
	clahe->apply(img, clahe_img);
	Mat clahe_hist_2 = histogram(clahe_img);
	imshow("Histogram for CLAHE 0.5 (10,10)", clahe_hist_2);
	imwrite("CLAHE_histogram_05(10,10).jpg", clahe_hist_2);
	imshow("�lahe 0.5 (10,10)", clahe_img);
	imwrite("CLAHE_2.jpg", clahe_img);

	clahe->setClipLimit(2);
	clahe->setTilesGridSize(Size(20, 20));
	clahe->apply(img, clahe_img);
	Mat clahe_hist_3 = histogram(clahe_img);
	imshow("Histogram for CLAHE 2 (20,20)", clahe_hist_3);
	imwrite("CLAHE_histogram_2(20,20).jpg", clahe_hist_3);
	imshow("�lahe 2 (20,20)", clahe_img);
	imwrite("CLAHE_3.jpg", clahe_img);

	//Global binarization
	int threshold = otsuThreshold(img);
	Mat otsu(img.rows, img.cols, CV_8UC1);
	Mat main_img = Mat::zeros(img.rows, img.cols * 2, CV_8UC1);
	for (int i = 0; i < img.rows; i += 1)
	{
		for (int j = 0; j < img.cols; j += 1)
		{
			if (img.at<uchar>(i, j) > threshold)
				otsu.at<uchar>(i, j) = 255;
			else
				otsu.at<uchar>(i, j) = 0;
		}
	}
	img.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	otsu.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Global binarization", otsu);
	imwrite("Global_binarization.jpg", otsu);
	imshow("Global binarization two images", main_img);
	imwrite("Global_binarization_two_img.jpg", main_img);

	//Local binarization
	float T = 0.15f;
	int part = 8;
	Mat local_bin = Mat::zeros(img.size(), CV_8UC1);
	thresholdIntegral(img, local_bin, T, part);
	img.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	local_bin.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Local binarization", local_bin);
	imwrite("Local_binarization.jpg", local_bin);
	imshow("Local binarization two images", main_img);
	imwrite("Local_binarization_two_img.jpg", main_img);

	//morphology filter
	Mat kernel = Mat::ones(2, 2, CV_32F);
	Mat res;
	//morphologyEx(local_bin, res, MORPH_CLOSE, kernel);
	morphologyEx(local_bin, res, MORPH_OPEN, kernel);
	local_bin.copyTo(main_img(Rect(0, 0, img.cols, img.rows)));
	res.copyTo(main_img(Rect(img.cols, 0, img.cols, img.rows)));
	imshow("Morphology filter for local binarization", res);
	imwrite("Filter_local_binarization.jpg", res);
	imshow("Morphology filter for local binarization two images", main_img);
	imwrite("Filter_local_binarization_two_img.jpg", main_img);

	//Alpha blending
	double alpha = 0.5;
	double beta = 1 - alpha;
	Mat alpha_blending;
	addWeighted(img, alpha, res, beta, 1.0, alpha_blending);
	imshow("Alpha blending", alpha_blending);
	imwrite("Alpha_blending.jpg", alpha_blending);

	waitKey(0);
	return 0;

}