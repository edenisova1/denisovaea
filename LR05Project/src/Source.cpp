#include <opencv2\highgui.hpp>
#include <iostream>
#include <opencv2\core.hpp>
#include <opencv2\core\types_c.h>
#include <opencv2\imgproc\imgproc.hpp>
#include <opencv2\highgui\highgui_c.h>

using namespace cv;
using namespace std;

static const int d = 100;
static const Rect2i rc_cell{ 0, 0, d, d };

void draw_cell(Mat& img, const int32_t x, const int32_t y,
	const Scalar color_ground, const Scalar color_figure) {
	Rect2i rc = rc_cell;
	rc.x += x;
	rc.y += y;
	rectangle(img, rc, color_ground, -1);
	circle(img, { rc.x + rc.width / 2, rc.y + rc.height / 2 }, rc.width / 5, color_figure, -1);
}




int main() {
	Mat img_src(200, 300, CV_8UC1);

	// draw source image
	draw_cell(img_src, 0, 0, { 255 }, { 127 });
	draw_cell(img_src, d, 0, { 127 }, { 0 });
	draw_cell(img_src, d * 2, 0, { 0 }, { 255 });
	draw_cell(img_src, 0, d, { 0 }, { 127 });
	draw_cell(img_src, d, d, { 255 }, { 0 });
	draw_cell(img_src, d * 2, d, { 127 }, { 255 });
	imshow("First img", img_src);
	imwrite("lab04.src.png", img_src);
	Mat matrix = Mat::zeros(3, 3, CV_8SC1);
	Mat matrix_T = matrix.clone();
	Mat res, res_T, img_res;
	matrix.at<int8_t>(0, 0) = 1;
	matrix.at<int8_t>(1, 0) = 2;
	matrix.at<int8_t>(2, 0) = 1;
	matrix.at<int8_t>(0, 2) = -1;
	matrix.at<int8_t>(1, 2) = -2;
	matrix.at<int8_t>(2, 2) = -1;
	matrix_T.at<int8_t>(0, 0) = 1;
	matrix_T.at<int8_t>(2, 0) = -1;
	matrix_T.at<int8_t>(0, 1) = 2;
	matrix_T.at<int8_t>(2, 1) = -2;
	matrix_T.at<int8_t>(0, 2) = 1;
	matrix_T.at<int8_t>(2, 2) = -1;

	filter2D(img_src, res, CV_32F, matrix, Point(-1, -1), 0, BORDER_REFLECT);
	filter2D(img_src, res_T, CV_32F, matrix_T, Point(-1, -1), 0, BORDER_REFLECT);
	
	Mat result_1, result_2;
	pow(res, 2, result_1);
	pow(res_T, 2, result_2);
	pow((result_1 + result_2), 0.5, img_res);
	img_res = (img_res + 255) / 2;
	img_res.convertTo(img_res, CV_8U);
	res = (res + 255) / 2;
	res.convertTo(res, CV_8U);
	res_T = (res_T + 255) / 2;
	res_T.convertTo(res_T, CV_8U);


	imshow("First res", res);
	imshow("Second res", res_T);
	imwrite("lab04.viz_dx.png", res);
	imwrite("lab04.viz_dy.png", res_T);
	imshow("Result", img_res);
	imwrite("lab04.viz_gradmod.png", img_res);

	waitKey(0);
	return 0;
}