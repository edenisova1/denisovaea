//#include <stdafx.h>
#include<stdio.h>
#include<math.h>
#include<time.h>
#include<opencv2\opencv.hpp>
#include<opencv2\highgui.hpp>
#include<opencv2\imgproc\imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

double sigma(Mat & m, int i, int j, int block_size)
{
	double sigma = 0;

	Mat m_tmp = m(Range(i, i + block_size), Range(j, j + block_size));
	Mat m_squared(block_size, block_size, CV_64F);

	multiply(m_tmp, m_tmp, m_squared);

	double avg = mean(m_tmp)[0];
	double avg_2 = mean(m_squared)[0];

	sigma = sqrt(avg_2 - avg * avg);

	return sigma;
}


double cov(Mat & m_1, Mat & m_2, int i, int j, int block_size)
{
	Mat m_3 = Mat::zeros(block_size, block_size, m_1.depth());
	Mat m_1_tmp = m_1(Range(i, i + block_size), Range(j, j + block_size));
	Mat m_2_tmp = m_2(Range(i, i + block_size), Range(j, j + block_size));


	multiply(m_1_tmp, m_2_tmp, m_3);

	double avg_ro = mean(m_3)[0];
	double avg_r = mean(m_1_tmp)[0];
	double avg_o = mean(m_2_tmp)[0];


	double covariance = avg_ro - avg_o * avg_r;

	return covariance;
}


double mse(Mat & img1, Mat & img2)
{
	int i, j;
	double mse = 0;
	int height = img1.rows;
	int width = img1.cols;

	for (i = 0; i < height; i++)
		for (j = 0; j < width; j++)
			mse += (img1.at<double>(i, j) - img2.at<double>(i, j)) * (img1.at<double>(i, j) - img2.at<double>(i, j));

	mse /= height * width;

	return mse;
}


double psnr(Mat & img_src, Mat & img_compressed, int block_size)
{
	int D = 255;
	return (10 * log10((D*D) / mse(img_src, img_compressed)));
}


double ssim(Mat & img_src, Mat & img_changed, int block_size, bool show_progress = false)
{
	double ssim = 0;
	float C1 = (0.01 * 255 * 0.01 * 255);
	float C2 = (0.03 * 255 * 0.03 * 255);

	int blocks_per_height = img_src.rows / block_size;
	int blocks_per_width = img_src.cols / block_size;

	for (int i = 0; i < blocks_per_height; i++)
	{
		for (int j = 0; j < blocks_per_width; j++)
		{
			int m = i * block_size;
			int n = j * block_size;

			double avg_o = mean(img_src(Range(i, i + block_size), Range(j, j + block_size)))[0];
			double avg_r = mean(img_changed(Range(i, i + block_size), Range(j, j + block_size)))[0];
			double sigma_o = sigma(img_src, m, n, block_size);
			double sigma_r = sigma(img_changed, m, n, block_size);
			double covariance = cov(img_src, img_changed, m, n, block_size);

			ssim += ((2 * avg_o * avg_r + C1) * (2 * covariance + C2)) / ((avg_o * avg_o + avg_r * avg_r + C1) * (sigma_o * sigma_o + sigma_r * sigma_r + C2));

		}
		if (show_progress)
			cout << "\r>>SSIM [" << (int)((((double)i) / blocks_per_height) * 100) << "%]";
	}
	ssim /= blocks_per_height * blocks_per_width;

	if (show_progress)
	{
		cout << "\r>>SSIM [100%]" << endl;
		cout << "SSIM : " << ssim << endl;
	}

	return ssim;
}

void metrics(Mat & img_src, Mat & img_changed, int block_size)
{
	img_src.convertTo(img_src, CV_64F);
	img_changed.convertTo(img_changed, CV_64F);

	int height_1 = img_src.rows;
	int width_1 = img_src.cols;

	if (height_1 % block_size != 0 || width_1 % block_size != 0)
	{
		cout << "WARNING : Image WIDTH and HEIGHT should be divisible by BLOCK_SIZE for the maximum accuracy" << endl;
	}

	double ssim_value = ssim(img_src, img_changed, block_size);
	double psnr_value = psnr(img_src, img_changed, block_size);

	cout << "SSIM : " << ssim_value << endl;
	cout << "PSNR : " << psnr_value << endl;
}


Mat LR_Filter(Mat &src_img, Mat &res_img, Mat &filter, int iterations)
{
	Mat Denom, Numer;
	do {
		filter2D(res_img, Denom, -1, filter, Point(-1, -1), 0, BORDER_DEFAULT);
		Numer = src_img / Denom;
		filter2D(Numer, Denom, -1, filter, Point(-1, -1), 0, BORDER_DEFAULT);
		res_img = res_img.mul(Denom);
		iterations--;
	} while (iterations > 0);
	return res_img;
}

int main(void)
{
	//Mat RSFilter
	int iterations_blurred, iterations_gaussian_blurred, iterations_box_filter_blurred;
	int iterations_blurred_distorted, iterations_gaussian_blurred_distorted, iterations_box_filter_blurred_distorted;
	int top, bottom, left, right;
	Scalar value;

	Mat RGB_Image[3], DFTTrans_Image[3];

	cout << "Enter The No. of Iteration for Lucy Richardson Filter for blurred image:" << endl;
	cin >> iterations_blurred;

	cout << "Enter The No. of Iteration for Lucy Richardson Filter for Gaussian blurred image:" << endl;
	cin >> iterations_gaussian_blurred;

	cout << "Enter The No. of Iteration for Lucy Richardson Filter for box-filter blurred image:" << endl;
	cin >> iterations_box_filter_blurred;

	cout << "Enter The No. of Iteration for Lucy Richardson Filter for distorted image with blur filter kernel:" << endl;
	cin >> iterations_blurred_distorted;

	cout << "Enter The No. of Iteration for Lucy Richardson Filter for distorted image with Gaussian filter kernel:" << endl;
	cin >> iterations_gaussian_blurred_distorted;

	cout << "Enter The No. of Iteration for Lucy Richardson Filter for distorted image with box filter kernel:" << endl;
	cin >> iterations_box_filter_blurred_distorted;

	Mat img = imread("start_text.jpg");
	imshow("Undistorted image", img);
	Mat distorted_img = imread("text_2.jpg");
	imshow("Distorted image", distorted_img);
	top = (int)(0.05*distorted_img.rows); bottom = (int)(0.05*distorted_img.rows);
	left = (int)(0.05*distorted_img.cols); right = (int)(0.05*distorted_img.cols);
	copyMakeBorder(distorted_img, distorted_img, top, bottom, left, right, BORDER_CONSTANT, value);

	Mat blurred, blurred_gaussian, blurred_box_filter;

	Mat blur_filter = (Mat_<float>(3, 3) << 0.0625, 0.125, 0.0625,
		0.125, 0.25, 0.125,
		0.0625, 0.125, 0.0625);

	Mat gaussian_filter = (Mat_<float>(5, 5) << 0.003, 0.013, 0.022, 0.013, 0.003,
		0.013, 0.059, 0.097, 0.059, 0.013,
		0.022, 0.097, 0.159, 0.097, 0.022,
		0.013, 0.059, 0.097, 0.059, 0.013,
		0.003, 0.013, 0.022, 0.013, 0.003);

	Mat box_filter = (Mat_<double>(5, 5) << 1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0,
		1.0, 1.0, 1.0, 1.0, 1.0);

	box_filter *= 0.04;

	filter2D(img, blurred, -1, blur_filter, Point(-1, -1), 0, BORDER_DEFAULT);
	GaussianBlur(img, blurred_gaussian, Size(5, 5), 1.0);
	blur(img, blurred_box_filter, Size(5, 5));

	imshow("Blurred image", blurred);
	imshow("Gaussian blurred image", blurred_gaussian);
	imshow("Box filter blurred image", blurred_box_filter);

	imwrite("blurred_img.jpg", blurred);
	imwrite("gaussian_blurred_img.jpg", blurred_gaussian);
	imwrite("box_filter_blurred_img.jpg", blurred_box_filter);

	cout << "Metrics' results for blurred image:" << endl;
	metrics(img, blurred, 9);
	cout << "Metrics' results for Gaussian blurred image:" << endl;
	metrics(img, blurred_gaussian, 9);
	cout << "Metrics' results for box-filter blurred image:" << endl;
	metrics(img, blurred_box_filter, 9);
	cout << "Metrics' results for distorted image:" << endl;
	metrics(img, distorted_img, 10);

	blurred.convertTo(blurred, CV_32F);
	blurred_gaussian.convertTo(blurred_gaussian, CV_32F);
	blurred_box_filter.convertTo(blurred_box_filter, CV_32F);
	distorted_img.convertTo(distorted_img, CV_32F);

	blurred = LR_Filter(blurred, blurred, blur_filter, iterations_blurred);
	blurred_gaussian = LR_Filter(blurred_gaussian, blurred_gaussian, gaussian_filter, iterations_gaussian_blurred);
	blurred_box_filter = LR_Filter(blurred_box_filter, blurred_box_filter, box_filter, iterations_box_filter_blurred);

	distorted_img = LR_Filter(distorted_img, distorted_img, blur_filter, iterations_blurred_distorted);
	distorted_img.convertTo(distorted_img, CV_8U);
	imshow("Result of distorted image with blur filter kernel", distorted_img);
	cout << "Metrics' results for corrected distorted image with blur filter kernel:" << endl;
	metrics(img, distorted_img, 9);
	imwrite("Distorted_res_blurred_img.jpg", distorted_img);

	distorted_img.convertTo(distorted_img, CV_32F);
	distorted_img = LR_Filter(distorted_img, distorted_img, gaussian_filter, iterations_gaussian_blurred_distorted);
	distorted_img.convertTo(distorted_img, CV_8U);
	imshow("Result of distorted image with gaussian filter kernel", distorted_img);
	cout << "Metrics' results for corrected distorted image with gaussian filter kernel:" << endl;
	metrics(img, distorted_img, 9);
	imwrite("Distorted_res_gaussian_blurred_img.jpg", distorted_img);

	distorted_img.convertTo(distorted_img, CV_32F);
	distorted_img = LR_Filter(distorted_img, distorted_img, box_filter, iterations_box_filter_blurred_distorted);
	distorted_img.convertTo(distorted_img, CV_8U);
	imshow("Result of distorted image with box filter kernel", distorted_img);
	cout << "Metrics' results for corrected distorted image with box filter kernel:" << endl;
	metrics(img, distorted_img, 9);
	imwrite("Distorted_res_box_filter_blurred_img.jpg", distorted_img);

	blurred.convertTo(blurred, CV_8U);
	blurred_gaussian.convertTo(blurred_gaussian, CV_8U);
	blurred_box_filter.convertTo(blurred_box_filter, CV_8U);

	imshow("Result of blurred image", blurred);
	cout << "Metrics' results for corrected blurred image:" << endl;
	metrics(img, blurred, 9);

	imshow("Result of Gaussian blurred image", blurred_gaussian);
	cout << "Metrics' results for corrected Gaussian blurred image:" << endl;
	metrics(img, blurred_gaussian, 9);

	imshow("Result of box filter blurred image", blurred_box_filter);
	cout << "Metrics' results for corrected box-filter blurred image:" << endl;
	metrics(img, blurred_box_filter, 9);

	imwrite("res_blurred_img.jpg", blurred);
	imwrite("res_gaussian_blurred_img.jpg", blurred_gaussian);
	imwrite("res_box_filter_blurred_img.jpg", blurred_box_filter);

	waitKey(0);
	return 0;
}